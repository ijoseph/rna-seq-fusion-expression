'''
Created on Jan 23, 2013

@author: Isaac C. Joseph (ijoseph@berkeley.edu)
'''


import StringIO
import argparse
import csv
import re
import sys

class FusionTranscriptMaker(object):
    '''
    Holds current transcriptome, and reads in information about gene fusions 
    found in a genome associated with the transcriptome.  
    Outputs new transcripts for transcript quantification in FASTA format   
    for use in downstream tools (e.g. eXpress). 
    
    Transcriptome should be downloaded from UCSC table browser so as to have the correct header 
    format.
    '''

    def __init__(self, transcriptomeFASTAFile, transcriptomeAnnotationFile, transcriptomeAnnotationFormat, \
                 fusionListSoftware, histograms, verbose, focusTranscriptsFile, GTFFile, twoBitGenomeFile, includeReciprocals, \
                 customDownstreamSequenceFile):
        '''
        Extracts information from transcript file. 
        '''
        
        # self.transIDToSequence: <String transcirptID> -> <String sequence>
        # self.transIDToChromCoor: <String transcriptID> -> <(String chromosome , (int chromosomalSart, int chromosomalEnd))>
        # self.transIDToStrand: <String transcriptID> -> <int strand>
        
        self.transIDToSequence = {}
        self.transIDToChromCoor = {}
        self.transIDToStrand = {}
        self.genomeVersion = ""
        self.genomeSource = ""
        self.repeatMasking = ""
        self.lineLength = -1
        self.histogramsFilename = histograms
        self.verbose = verbose
        self.focusTranscriptsFile = focusTranscriptsFile
        self.twoBitGenomeFile = twoBitGenomeFile
        self.includeReciprocals = includeReciprocals        
        
        
        # Parse FASTA transcriptome bases
        (self.transIDToSequence, self.transIDToChromCoor, self.transIDToStrand, self.genomeVersion, self.genomeSource, self.repeatMasking, self.lineLength) = \
            self.parseTranscriptomeFASTA(transcriptomeFASTAFile)
        
        # Parse transcriptome annotation file 
        (self.geneToTranscriptIDToExonList, self.geneToTranscriptIDToStartEnd) = self.parseTranscriptomeAnnotation(transcriptomeAnnotationFile, transcriptomeAnnotationFormat)
        # self.geneToTranscriptIDToExonList: <String geneID> -> <String transcriptID> -> <[(exon1StartBase, exon2EndBase),...(exonNStartBase, exonNEndBase)]> (note: exon1 is not necessarily the first exon of the transcript, but is the first exon genomically)
        # self.geneToTranscriptIDToStartEnd: <String geneID> -> <String transcriptID> -> (transcriptStartBase, transriptEndBase)
        
        # Parse custom downstream sequence file if relevant
        if customDownstreamSequenceFile is not None:
            self.customDownstreamSequence= self.parseCustomDownstreamSequenceFile(customDownstreamSequenceFile)
        else:
            self.customDownstreamSequence = None
        
        # Create fusion output parser for appropriate software.        
        if fusionListSoftware == 'deFuse':
            self.fusionOutputParser = DeFuseFusionOutputParser(self.geneToTranscriptIDToExonList, self.geneToTranscriptIDToStartEnd,  True if self.customDownstreamSequence is not None else False)
        elif fusionListSoftware == 'PRADA':
            # Parse GTF for name conversion, too
            HGNCNametoENSName = self.parseGTF(GTFFile)
            self.fusionOutputParser = PRADAFusionOutputParser(self.geneToTranscriptIDToExonList, self.geneToTranscriptIDToStartEnd, HGNCNametoENSName, True if self.customDownstreamSequence is not None else False)            
        else:
            raise NotImplementedError("Software output %s not implemented yet" %(fusionListSoftware))
        

        self.upstreamFusionSoftware = fusionListSoftware        
        
            
    ''' Parse Input Sequence Files '''
        
    def parseTranscriptomeFASTA(self, transFile):
        '''
        Modified from Adam Roberts. Parses transcriptome, makes transcriptID -> string dictionary, 
        transcriptID to chromosomal coordinates dictionary, transcriptID to strand dictionary
        '''        
        sys.stderr.write( "Preparing to create fusion transcripts by loading transcriptome fasta...")    
        
        transFile.seek(0)
        
        def parseHeaderLine(line):
            '''
            Used for parsing the header line to validate form, acquire transcript name, chromosomal coordinates, and strand information. 
            '''
            
            # match something like: ">hg19_ensGene_ENST00000237247 range=chr1:66999066-67210057 5'pad=0 3'pad=0 strand=+ repeatMasking=none"
            
            headerRegex=">hg\d\d_[^\s]+Gene_[^\s]+\srange=chr[^\s]+:\d+-\d+\s5'pad=0\s3'pad=0\sstrand=[+-]\srepeatMasking=[^\s]+"
            if not re.match(headerRegex, line):
                exampleString = ">hg19_ensGene_ENST00000237247 range=chr1:66999066-67210057 5'pad=0 3'pad=0 strand=+ repeatMasking=none"
                print "Warning: transcriptome transcript name does not match UCSC standardized format; name is \n %s , but should be something like \n %s \n" %(line, exampleString)
            
            targetLineArray = line[1:].strip().split()
            (genomeVersion, genomeSource, transcriptID) = targetLineArray[0].split('_') # target name is the transcript name
            
            chromosomeAndCoordinates = targetLineArray[1].split('=')[1] 
            coordinates =tuple([int(strCoor) for strCoor in chromosomeAndCoordinates.split(':')[1].split("-")]) # chromosomal coordinates: (chromsomalStart, chromosomalStop) 
            


            
            chromosome = chromosomeAndCoordinates.split(':')[0] # chr1, etc.

            # check that second coordinate number is larger than the first one
            assert coordinates[1] >= coordinates[0], "transcriptome transcript coordinates must be by genome strand; transcript %s violates this (coordinates are %s:%u-%u) " %(chromosome, coordinates[0], coordinates[1])                 
            
            strand = 1 if targetLineArray[4].split('=')[1] == '+' else -1 # 1 for plus strand, -1 for minus strand      
            
            repeatMasking  = targetLineArray[5].split('=')[1] # 'none' or some other option. string.            
            
            
            return (transcriptID, (chromosome, coordinates), strand, genomeVersion, genomeSource, repeatMasking) 
        
        
        transIDToSequence = {}
        transIDToCoor = {}
        transIDToStrand = {}
        genomeVersion = ""
        genomeSource = ""
        repeatMasking = ""
        lineLength = 0
                        
        targ = ''
        seq = ''
        
        for line in transFile:            
            if line[0] == '>':
                if targ != '':
                    transIDToSequence[targ] = seq
                    
                # Parse header line                
                (targ, transIDToCoor[targ], transIDToStrand[targ], newGenomeVersion, newGenomeSource, newRepeatMasking) = parseHeaderLine(line)
                
                
                # Warnings 
                # We've already seen this transcript ID
                if targ in transIDToSequence:
                    print "WARNING: Target '{0}' is repeated in FASTA file.".format(targ)
                    
                # Genome version, source, or repeat masking has changed
                
                if genomeVersion and genomeVersion != newGenomeVersion and genomeSource != newGenomeSource and repeatMasking != newRepeatMasking:
                    print "Warning: genome data (genome version, genome source, and/or repeat masking) has changed between transcripts from %s %s %s to %s %s %s" %(genomeVersion, genomeSource, repeatMasking, newGenomeVersion, newGenomeSource, newRepeatMasking)
                (genomeVersion, genomeSource, repeatMasking) = newGenomeVersion, newGenomeSource, newRepeatMasking                    
                
                
                
                seq = ''
            else:
                seq += line.strip()
                lineLength = len(line) if len(line) > 0 else lineLength
                
                if targ != '':
                    transIDToSequence[targ] = seq
        sys.stderr.write("finished.\n")
        return (transIDToSequence, transIDToCoor, transIDToStrand, genomeVersion, genomeSource, repeatMasking, lineLength)
    
    def parseTranscriptomeAnnotation(self, transcriptomeAnnotationFile, transcriptomeAnnotationFormat):
        transcriptomeAnnotationFile.seek(0)
        sys.stderr.write( "Preparing to create fusion transcripts by loading transcriptome annotation...")    
        if (transcriptomeAnnotationFormat == 'Ensembl'):
            # Create dictionaries from 
            geneToTranscriptIDToExonList = {} # gene ID -> list (transcript IDs) -> each to exon list
            geneToTranscriptIDToStartEnd = {}            
            
            for line in transcriptomeAnnotationFile:
                if (line.startswith("#")) or (line.strip() == ""):
                    continue
                
                try:
                    (_, transcriptName, _, _, tsStart, tsEnd, _, _, _,  exonStarts, exonEnds, _, geneName, _, _, _ ) = line.split()
                except ValueError:
                    raise NotImplementedError("Transcriptome annotation format appears to not be Ensembl, and thus is not implemented yet")
                # (add 1 to the start index below since UCSC-downloaded Ensembl transcriptome annotation off-by-one in terms of starts from Ensembl-downloaded Ensembl GTF annotation)
                exonList = zip([int(start)+1 for start in exonStarts.split(',')[0:-1]], [int(end) for end in exonEnds.split(',')[0:-1]]) #[(exonStartIndex, exonEndIndex)] 
                
                if geneToTranscriptIDToExonList.has_key(geneName):
                    geneToTranscriptIDToExonList[geneName][transcriptName] = exonList
                    geneToTranscriptIDToStartEnd[geneName][transcriptName] = (int(tsStart) +1, int(tsEnd))
                else:
                    geneToTranscriptIDToExonList[geneName] = {transcriptName:exonList}
                    geneToTranscriptIDToStartEnd[geneName] = {transcriptName: (int(tsStart) +1, int(tsEnd))}
        else:
            raise NotImplementedError("Transcriptome annotation format %s not implemented yet" %(transcriptomeAnnotationFormat))
            # TODO: transcriptome annotation GAF, UCSC        
        sys.stderr.write("finished.\n")
        return (geneToTranscriptIDToExonList, geneToTranscriptIDToStartEnd)

    def parseGTF(self, GTFFile):
        '''
        Reads in GTF File and creates <HGNC name> -> < ENSG Name> map
        '''
        
        GTFFile.seek(0)
        sys.stderr.write("Parsing GTF file...")
        hgncNameToENSGName = {}
        ensgNameToHGNCName = {}        
        reader = csv.reader(GTFFile, delimiter = "\t")        
        for row  in reader:
            info = row[8].split(";")
            hgncName = info[3].split('"')[1]
            ensgName = info[0].split('"')[1]
            # Update HGNC -> ENSG
            if not hgncNameToENSGName.has_key(hgncName): # we've already seen this common gene name                
                hgncNameToENSGName[hgncName] = ensgName
                
            # Update ENSG -> HGNC
            if not ensgNameToHGNCName.has_key(ensgName):
                ensgNameToHGNCName[ensgName] = hgncName
        
        sys.stderr.write("finished.\n") 
        return hgncNameToENSGName
    
    def parseCustomDownstreamSequenceFile(self, customDownstreamSequenceFile):
        sys.stderr.write( "Parsing custom downstream sequence...")    
        buff = StringIO.StringIO()
        for line in customDownstreamSequenceFile:
            if line.startswith(">"):
                continue
            buff.write(line.strip())
        sys.stderr.write("done.")            
        return buff.getvalue()
            
                        
    
    ''' Create new transcripts '''
            
    def updateTranscriptome(self, fusionListFile, output):
        '''
        Takes in list of Fusion objects; uses these to create new transcript(s)
        for the updated transcriptome.  
        '''

        # set output file handle to standard out if no output file specified; else
        # set to specified file
        if output is None:
            outputFH = sys.stdout
            
        elif type(output) == type(StringIO.StringIO()):
            outputFH = output         
        else:            
            outputFH = open(output, 'w')
        
        if type(fusionListFile) == file: # if given a file, parse that file;
            (fusionList, skippedDict, needFullGenome) = self.fusionOutputParser.parseOutput(fusionListFile, self.verbose) # parse the output, using type polymorhpism
        else:
            assert type(fusionListFile) == list, "Unknown fusion list input type (neither list nor file); type is " + str(type(fusionListFile))            
            fusionList = fusionListFile
            skippedDict = {}
            # Check to see if we need full genome (by seeing if there are intron or upstream/downstream (intergernic) fusion breakpoints
            for fusion in fusionList:
                if ('intron' in (fusion.breakpointLoc1, fusion.breakpointLoc2)) or ('upstream' in (fusion.breakpointLoc1, fusion.breakpointLoc2)) or ('downstream' in (fusion.breakpointLoc1, fusion.breakpointLoc2)):
                    needFullGenome=True                     
                else: 
                    needFullGenome = False
        
        skipFullGenomeBreakpointFusions = True # skip breakpoints that require the full genome unless we have loaded the full genome sequence .2bit file
        if needFullGenome:
            if self.twoBitGenomeFile is None:
                sys.stderr.write("Warning: not creating fusions with genomic breakpoints in intronic or intergenic regions because no .2bit genome file specified (specify one with --twoBitGenomeFile or -g command line arguments")
            else:
                try: 
                    import twobitreader
                    self.genome = twobitreader.TwoBitFile(self.twoBitGenomeFile.name) 
                    skipFullGenomeBreakpointFusions = False
                except ImportError:
                    sys.stderr.write("Warning: not creating fusions with genomic breakpoints because python twobitreader package not installed")
                    
        if (self.histogramsFilename is not None):
            self._plotNumFusIsoformsHist(fusionList)
            
        
        writtenFusionSequences = set() # set of fusions already written, based on sequence        
        
        # Add the reciprocal of each fusion for creation
        if self.includeReciprocals != "":
            fusionList += self._addReciprocals(fusionList)
        
        for fusion in fusionList:
            
            # Check if need full genome, and skip this fusion if we do and don't have it
            if skipFullGenomeBreakpointFusions and \
            (('intron' in fusion.breakpointLoc1)\
             or ('upstream' in fusion.breakpointLoc1)\
             or ('downstream' in fusion.breakpointLoc1)\
             or (self.customDownstreamSequence is not None and ( 'intron' in (fusion.breakpointLoc2)\
             or ('upstream' in  fusion.breakpointLoc2)\
             or ('downstream' in fusion.breakpointLoc2)))):            
                try:
                    skippedDict["fullGenomeSequenceNeeded"] +=1
                except KeyError:
                    skippedDict["fullGenomeSequenceNeeded"] = 1
                continue
                
            # Grab the sequence of each contained fusion from the dictionary            
            # Try to get the upstream sequence
            try:
                transcript1Seq = self.transIDToSequence[fusion.transcript1ID]
            except KeyError:
                if self.verbose:
                    sys.stderr.write( "Transcript ID %s in fusion not contained in transcriptome fasta. Skipping fusion %s|%s\n" %(fusion.transcript1ID, fusion.transcript1ID, fusion.transcript2ID))
                try: 
                    skippedDict["transcriptNotFound"] +=1
                except KeyError:
                    skippedDict["transcriptNotFound"] = 1                
                continue
            
            # Try to get the downstream sequence                        
            try:
                transcript2Seq = self.transIDToSequence[fusion.transcript2ID] if self.customDownstreamSequence is None else ""
            except KeyError:
                try: 
                    skippedDict["transcriptNotFound"] +=1
                except KeyError:
                    skippedDict["transcriptNotFound"] = 1  
                if self.verbose:
                    sys.stderr.write(  "Transcript ID %s in fusion not contained in transcriptome fasta. Skipping fusion %s|%s\n" %(fusion.transcript2ID, fusion.transcript1ID, fusion.transcript2ID))
                continue            
            

            (fusionTSSeq, fusionTSGenomicCoor, ts1RelativeBreakPosition, ts2RelativeBreakPosition) = self._createFusionTranscript(fusion, transcript1Seq, transcript2Seq)
            if fusion.orientation == "ff":

                # Don't re-add redundant fusion transcripts
                if fusionTSSeq in writtenFusionSequences: 
                    if self.verbose:
                        sys.stderr.write("Skipping fusion transcript isoform" + str(fusion) + " due to sequence redundancy with another fusion transcript isoform in this gene pair\n")
                else:
                    writtenFusionSequences.add(fusionTSSeq)
                    
                    try:
                        skippedDict["transcriptsWritten"] +=1
                    except KeyError:
                        skippedDict["transcriptsWritten"] = 1
                        
                                        
                    # Write the fusion transcript to the relevant file(s)
                    self._writeFusionTranscript(fusion, fusionTSSeq, fusionTSGenomicCoor, ts1RelativeBreakPosition, ts2RelativeBreakPosition, outputFH)
                    if self.focusTranscriptsFile: # write fusion transcript also for focusing on downstream by ReXpress-focus
                        self._writeFusionTranscript(fusion, fusionTSSeq, fusionTSGenomicCoor, ts1RelativeBreakPosition, ts2RelativeBreakPosition, self.focusTranscriptsFile)
                        

        outputFH.close()
        
        # Write constituent transcripts to the focusTranscirpt file if indicated
        if self.focusTranscriptsFile:            
            transcriptIDSet = set([fus.transcript1ID for fus in fusionList] + [fus.transcript2ID for fus in fusionList]) # all constituent transcript IDs
            for transcriptID in transcriptIDSet:
                self._writeTranscript(transcriptID)
                
        self._outputFusionStats(skippedDict)        

    def _createFusionTranscript(self, fusion, transcript1Seq, transcript2Seq):
        '''
        Takes the transcript-relative breakpoint positions of each transcript and each sequence to create fusion transcript
        (including the intronic regions if relevant)  
        '''
        # Find relative position of break on each transcript via knowledge of exons 
        (ts1RelativeBreakPosition, lastExonBorder1, fusion.exonNum1) = self._genomicToTS1Indices(fusion.ensGeneID1, fusion.transcript1ID, fusion.geneStrand1, fusion.transcript1GenomicBreakPos)
        (ts2RelativeBreakPosition, lastExonBorder2, fusion.exonNum2) = self._genomicToTS2Indices(fusion.ensGeneID2, fusion.transcript2ID, fusion.geneStrand2, fusion.transcript2GenomicBreakPos) if self.customDownstreamSequence is None else (-1,-1,-1) 
        
        # Create fusion transcript using relative breakpoints
        
        upstreamSequence = transcript1Seq[0:ts1RelativeBreakPosition]
        downstreamSequence =  transcript2Seq[ts2RelativeBreakPosition:] if self.customDownstreamSequence is None else self.customDownstreamSequence 
                                        
        if fusion.breakpointLoc1 == 'intron': # Add region between breakpoint and exon #TOOD: remove the temporary intron-exon junctions
            if fusion.geneStrand1 == '+':
                upstreamSequence += "<>" +  self.genome[fusion.chrom1][lastExonBorder1:fusion.transcript1GenomicBreakPos] # add region from last exon end to breakpoint
            elif fusion.geneStrand1 == '-':
                upstreamSequence += "<>" + self._reverseComplement(self.genome[fusion.chrom1][fusion.transcript1GenomicBreakPos:lastExonBorder1]) # add region from last exon end to breakpoint (on minus strand)
            else:
                sys.stderr.write("unknown strand of upstream fusion")
                sys.exit(3)
        
        if self.customDownstreamSequence is None and fusion.breakpointLoc2 == 'intron':
            if fusion.geneStrand2 == '+':
                downstreamSequence = self.genome[fusion.chrom2][fusion.transcript2GenomicBreakPos:lastExonBorder2] + "<>" +   downstreamSequence # add region between breakpoint and first contained exon
                if self.verbose:
                    sys.stderr.write("Adding {0} bases of intronic sequence between downstream breakpoint ({1}) and first exon of downstream transcript isoform ({2}) in fusion\n{3}\n"\
                                     .format(lastExonBorder2 - fusion.transcript2GenomicBreakPos, fusion.transcript2GenomicBreakPos, lastExonBorder2, fusion))                
            elif fusion.geneStrand2 == '-':
                downstreamSequence = self._reverseComplement(self.genome[fusion.chrom2][lastExonBorder2:fusion.transcript2GenomicBreakPos]) + "<>" + downstreamSequence                
            else:
                sys.stderr.write("unknown strand of downstream fusion")
                sys.exit(3)        
        fusionTSSeq =  upstreamSequence + downstreamSequence  # fusion transcript sequence        
        
        
        # Calculate genomic coordinates
        fusionTSGenomicCoor = self._calculateCoordinates(fusion)                                                         
        
        return (fusionTSSeq, fusionTSGenomicCoor, ts1RelativeBreakPosition, ts2RelativeBreakPosition)

    def _addReciprocals(self, fusionList):
        '''
        Adds reciprocals to the fusion list according to the genes specified for making reciprocals in self.includeReciprocals
        '''        
        outList = []
        if len(self.includeReciprocals) == 1 and self.includeReciprocals[0].lower() == 'all': # include all reciprocals
            outList += [fusion.reciprocal() for fusion in fusionList]        
            return outList
        
        
        for recipGeneName in self.includeReciprocals:
            if "|" not in recipGeneName:            
                for fusion in fusionList:
                    if fusion.gene1Name.lower() == recipGeneName.lower() or fusion.gene2Name.lower() == recipGeneName.lower(): # one of the fusion's constituent gene names matches a gene name wanted for reciprocals  
                        outList += [fusion.reciprocal()]
                        break # now try the next reciprocal gene
            else:
                for fusion in fusionList:
                    if fusion.gene1Name.lower() + "|" + fusion.gene2Name.lower() == recipGeneName.lower(): # if both upstream and downstraem gene are specified, add this to the output list
                        outList += [fusion.reciprocal()]
                        break # now try the next reciprocal 
        
        
        return outList
    
    def _reverseComplement(self, sequence):        
        table = {'A':'T', 'a':'t', \
                 'T': 'A', 't':'a', \
                 'G':'C', 'g':'c', \
                 'C':'G', 'c': 'g', \
                 'N':'N', 'n':'n'}
        return "".join([table[base] for base in sequence[::-1]])
        
    ''' Convert Breakpoint Indices '''
                
    def _genomicToTS1Indices(self, ensGeneID, tsID, geneStrand, genomicBreakPosition):
        '''
        Finds transcript-relative index on transcript 1 of genomic breakpoint 
        '''
        
        exonList = self.geneToTranscriptIDToExonList[ensGeneID][tsID]
        
        distance = 0        
        
        if geneStrand == "+":
            for (exonNum, (exonStart, exonEnd)) in enumerate(exonList): 
                if exonEnd <= genomicBreakPosition: # genomic break is after the end of this exon. keep going  
                    distance += (exonEnd - exonStart + 1) 
                elif exonStart < genomicBreakPosition < exonEnd: # genomic break is within this exon: exonic break
                    distance += (genomicBreakPosition - exonStart) + 1                    
                    return (distance, exonEnd, exonNum +1) 
                else: # genomic break was after the end of the previous exon but before the beginning of this exon. intronic break. report last exon end for intronic sequence purposes. 
                    return (distance, exonList[exonNum-1][1], exonNum) # previous exon's end (last exon included; want sequence from this to breakpoint) 
        else: # - strand
            for (exonNum, (exonStart, exonEnd)) in enumerate(reversed(exonList)): 
                if exonStart >= genomicBreakPosition: # genomic break is before the start of this exon. keep going backwards on genome...
                    distance += (exonEnd - exonStart + 1)                    
                elif exonStart < genomicBreakPosition < exonEnd: # genomic break is within this exon 
                    distance += (exonEnd - genomicBreakPosition) + 1
                    return (distance, exonStart, exonNum+1)                    
                else: # genomic break was before the start of the previous exon but is after the end of this exon. return previous exonStart
                    return (distance, list(reversed(exonList))[exonNum-1][0], exonNum)                    
                        
    def _genomicToTS2Indices(self, ensGeneID, tsID, geneStrand, genomicBreakPosition):
        '''
        Finds transcript-relative index on transcript 1 of genomic breakpoint
        '''
        if self.customDownstreamSequence is not None:
            return (-1, -1, -1)
        
        exonList = self.geneToTranscriptIDToExonList[ensGeneID][tsID]        
        distance = sum([(end - start + 1) for (start, end) in exonList]) # start at full distance on transcript
        if geneStrand == "+":
            for (exonNum, (exonStart, exonEnd)) in enumerate(reversed(exonList)): 
                if exonStart >= genomicBreakPosition: # genomic break is before the start of this exon... keep going backwards on the genome
                    distance -= (exonEnd - exonStart + 1) 
                elif exonStart < genomicBreakPosition < exonEnd: # genomic break is within this exon. 
                    distance -= (exonEnd - genomicBreakPosition + 1)
                    return (distance, exonStart, len(exonList) - exonNum)                    
                else: # genomic break was before the start of the previous exon but after the end of this exon. intronic break. return previous exon start
                    return (distance, list(reversed(exonList))[exonNum -1][0], len(exonList) - exonNum +1)                
                                    
        else: # - strand
            for (exonNum, (exonStart, exonEnd)) in enumerate(exonList):
                if exonEnd - 1 <= genomicBreakPosition: # genomic break is after the end of this exon... keep going...
                    distance -= ((exonEnd - exonStart) + 1)                    
                elif exonStart < genomicBreakPosition < exonEnd -1: # genomic break is exonic: return this exon's end
                    distance -= ((genomicBreakPosition - exonStart) +1 )
                    return (distance, exonEnd, len(exonList ) - exonNum)                    
                else: # genomic break was after the end of the previous exon but before the start of this exon. intronic break. return the end of the previous exon
                    return (distance, exonList[exonNum-1][1], len(exonList) - exonNum +1)
    
    def _calculateCoordinates(self, fusion):
        '''
        Calculate the chromosomal coordinates of the two fusion transcripts based on the orientation of the constitutent transcripts 
        and the relative location of the breakpoint on both transcripts.  
        '''
        (ts1chromName, (ts1Start, ts1End)) = self.transIDToChromCoor[fusion.transcript1ID] #chromosomal coordinates of first transcript in the fusion
        (ts2chromName, (ts2Start, ts2End)) = self.transIDToChromCoor[fusion.transcript2ID] if self.customDownstreamSequence is None else ("custom", (-1,-1))# chromosomal coordinates in second transcript in the fusion                
               
        # Figure out the genomic coordinates for the fusion transcripts based on the strand of each constituent transcript
        # Format is (chromosome name, start, end, chromosome name, start, end)
        fusionTSCoor = []
        if self.transIDToStrand[fusion.transcript1ID] == 1:
            fusionTSCoor += [ts1chromName, ts1Start, fusion.transcript1GenomicBreakPos]
        
        elif self.transIDToStrand[fusion.transcript1ID] == -1:
            fusionTSCoor += [ts1chromName, fusion.transcript1GenomicBreakPos, ts1End]
        else:
            sys.stderr.write( "Transcript orientation issue for upstream transcript in fusion (transcript {0}); check transcriptome transcript names ({0})for correct syntax. Exiting.".format(fusion.transcript1ID))
            sys.exit(2)
            
        if self.customDownstreamSequence is None:
            if self.transIDToStrand[fusion.transcript2ID] == 1:
                fusionTSCoor += [ts2chromName, fusion.transcript2GenomicBreakPos, ts2End]
            elif self.transIDToStrand[fusion.transcript2ID] == -1:
                fusionTSCoor +=   [ts2chromName, ts2Start, fusion.transcript2GenomicBreakPos]
            else:
                sys.stderr.write( "Transcript orientation issue for downstream in fusion (transcript {0}); check transcriptome transcript names ({0})for correct syntax. Exiting.".format(fusion.transcript2ID))
                sys.exit(2)
        else: # custom downstream sequence, for which we don't know the start or the downstream break position
            fusionTSCoor += ["custom", -1, -1]        
        
        return fusionTSCoor
    
    ''' Plot Transcript Information '''
    
    def _plotNumFusIsoformsHist(self, fusionList):
        try:
            import matplotlib
            matplotlib.use('Agg') # for use on servers without X11 forwarding
            import matplotlib.pyplot as p
            import numpy
        except ImportError:
            sys.stderr.write("Error: matplotlib or numpy not found (or could not be imported), so not making histograms")
            return
        
        genePairNumberToNumPairs = {}
        for fusion in fusionList:
            genePairNumberToNumPairs[fusion.genePairNumber] = fusion.totalNumberOfTranscriptPairs
            
                    
        try:
            p.hist(genePairNumberToNumPairs.values())
        except ValueError:
            p.hist(genePairNumberToNumPairs.values(), range=[0, genePairNumberToNumPairs.values()[0]])
        p.xlabel("$Z = $ Number of fusion transcript pairs spanning each fusion junction $j_1, ... , j_n$")
        p.ylabel("Frequency")
        p.title(r"$\overline{Z} = %.1f$, $s = %.1f$ (sample standard deviation), $\sum_{i=1}^n Z_i = %u$, $n = %u$"\
                %(numpy.mean(genePairNumberToNumPairs.values()), numpy.std(genePairNumberToNumPairs.values()), sum(genePairNumberToNumPairs.values()), len(genePairNumberToNumPairs.keys())))
        p.savefig(self.histogramsFilename.name, format="pdf")

    def _plotTranscriptLengthDistribution(self):
        '''
        Plots a histogram of the length distribution of the transcripts in the 
        input transcriptome using matplotlib
        '''
        try:
            import matplotlib
            matplotlib.use('Agg') # for use on servers without X11 forwarding
            import matplotlib.pyplot as p
        except ImportError:
            sys.stderr.write("Error: matplotlib not found (or could not be imported), so not making histogramsFilename")
            return
        
        lengths = [len(val) for val in self.transIDToSequence.values()]
        p.hist(lengths)
        
        
        for (index, (transID, sequence)) in enumerate(self.transIDToSequence.items()):
            if len(sequence) <=50:
                print "Index %u , Transcript ID %s, Sequence %s" %(index, transID, sequence)
        p.savefig("lengths-histogram.pdf", format="pdf")

    def _outputFusionStats(self, skippedDict):
        '''
        Looks at skippedDict ( <String reason> -> <int numberSkipped>) and outputs bowtie-style report.  
        '''
        output = ""
        if len(skippedDict) > 1:        
            output += "{0:,} total gene fusion junctions read from input file; of these:\n".format(skippedDict["totalFusionJunctions"])
            
            notBreakUpstreamOrDownstream = skippedDict["totalFusionJunctions"] - skippedDict["breakUpstreamOrDownstream"]
            output+= "\t {0:,} ( {1:.2%} ) had breakpoints within the annotated transcribed region of their constituent genes; of these:\n"\
            .format(notBreakUpstreamOrDownstream, float(notBreakUpstreamOrDownstream)/skippedDict["totalFusionJunctions"]) if skippedDict["totalFusionJunctions"] !=0 else 0            
            
            
            notInconsistentDirectionality = notBreakUpstreamOrDownstream - skippedDict["inconsistentDirectionality"]
            output+="\t\t{0:,} ( {1:.2%} ) had spanning read alignment strand consistent with transcript directionality on both upstream and downstream constituent genes; of these:\n" \
            .format(notInconsistentDirectionality, float(notInconsistentDirectionality)/notBreakUpstreamOrDownstream if notBreakUpstreamOrDownstream !=0 else 0)
                
            
            inAnnotation = notInconsistentDirectionality - skippedDict["geneNotInAnnotation"]
            output+="\t\t\t{0:,} ( {1:.2%} ) had constituent genes that were found in the input transcriptome annotation; of these:\n" \
            .format(inAnnotation, float(inAnnotation)/notInconsistentDirectionality if notInconsistentDirectionality !=0 else 0)
                
                
            notNoSpanningTranscripts = inAnnotation - skippedDict["noSpanningTranscripts"]
            output+="\t\t\t\t{0:,} ( {1:.2%} ) were \"valid:\" had transcripts spanning the gene-level fusion junction for both constituent genes in the input transcriptome  (fusion transcript isoforms output for only these)\n"\
            .format(notNoSpanningTranscripts, float(notNoSpanningTranscripts)/inAnnotation if inAnnotation !=0 else 0)      
            
            output += "{0:,} fusion transcript isoforms written for an average of {1:.2f} possible fusion transcript isoforms per gene fusion junction\n"\
            .format(skippedDict["transcriptsWritten"], float(skippedDict["transcriptsWritten"])/ notNoSpanningTranscripts if notNoSpanningTranscripts !=0 else 0)
        else:
            try:
                output +="{0:,} fusion transcripts written\n"\
                .format(skippedDict["transcriptsWritten"])
            except KeyError:
                output += "0 fusion transcripts written\n"
        
        sys.stderr.write(output)
         
    ''' Output Transcripts '''    
        
    def _writeFusionTranscript(self, fusionObj, fusionSequence, fusionTSCoor, tsRelativeBreakpointPosition, ts2RelativeBreakpointPosition, outputFH):
        '''
        Appends to the output file handle the new transcripts.        
        '''
        
        # write header            
        outputFH.write(">%s_%s_%s|%s_%s-gene-pair-%06d_(%s|%s):transcript-pair-%06d_transcript-relative-break-positions-%u|%u range=%s:%u-%u|%s:%u-%u 5'pad=0 3'pad=0 strand=%s|%s repeatMasking=%s\n" %(self.genomeVersion, self.genomeSource, \
                                                                                                           fusionObj.transcript1ID, fusionObj.transcript2ID, \
                                                                                                           self.upstreamFusionSoftware, fusionObj.genePairNumber, \
                                                                                                           fusionObj.gene1Name, fusionObj.gene2Name,\
                                                                                                           fusionObj.geneTranscriptPairNumber,\
                                                                                                            tsRelativeBreakpointPosition, ts2RelativeBreakpointPosition,\
                                                                                                           fusionTSCoor[0], fusionTSCoor[1], fusionTSCoor[2], \
                                                                                                           fusionTSCoor[3], fusionTSCoor[4], fusionTSCoor[5], \
                                                                                                           "+" if self.transIDToStrand[fusionObj.transcript1ID] == 1 else "-" , 
                                                                                                           "+" if self.transIDToStrand[fusionObj.transcript2ID] == 1 else "-", 
                                                                                                           self.repeatMasking)) 
        i = 0
        fusionTSLength = len(fusionSequence) # fusionObj transcript length
        while i < fusionTSLength: # loop through fusionObj transcript sequence, outputting with new lines in same place as input transcriptome
            outputFH.write(fusionSequence[i:min(i+self.lineLength, fusionTSLength)] + "\n")
            i += self.lineLength
        
    def _writeTranscript(self, transcriptID): 
        # write header of the form
        # #">hg19_ensGene_ENST00000237247 range=chr1:66999066-67210057 5'pad=0 3'pad=0 strand=+ repeatMasking=none"
        try:
            (chromName, (start, end)) = self.transIDToChromCoor[transcriptID]
        except KeyError: # transcript not found
            return 
        self.focusTranscriptsFile.write(">{0}_{1}_{2} range={3}:{4}-{5} 5'pad=0 3'pad=0 strand={6} repeatMasking={7}\n" \
                                        .format(self.genomeVersion, self.genomeSource, transcriptID, chromName, 
                                                start, end, "+" if self.transIDToStrand[transcriptID] == 1 else "-", self.repeatMasking))
        i = 0
        seqLen = len(self.transIDToSequence[transcriptID])
        while i < seqLen:
            self.focusTranscriptsFile.write(self.transIDToSequence[transcriptID][i:min(i+self.lineLength, seqLen)] + "\n")
            i += self.lineLength
        
class Fusion(object):
    '''
    Class for fusion objects. Holds the transcript IDs for the 
    two constituent transcripts, the genomic break positions on each transcript, 
    and the directionality of the fusion with respect to the transcripts
    '''
    def __init__ (self, ensGeneID1, geneStrand1, transcript1ID, chrom1, transcript1GenomicBreakPos, ensGeneID2, \
                  geneStrand2, transcript2ID, chrom2, transcript2GenomicBreakPos, genePairNumber = 1, \
                  gene1Name = "", gene2Name = "", breakpointLoc1 = "transcribed", breakpointLoc2 = "transcribed", \
                  exonNum1 = -1, exonNum2 = -1, \
                  geneTranscriptPairNumber = 1, totalNumberOfTranscriptPairs = 1, orientation ="ff", customDownstream = False):
         
        self.transcript1ID = transcript1ID # ID of transcript 1. If Ensembl, something like ENST00000...
        self.ensGeneID1 = ensGeneID1 
        self.geneStrand1 = geneStrand1        
        self.transcript1GenomicBreakPos = transcript1GenomicBreakPos # genomic break position of transcript 1, 0-indexed
        self.chrom1 = chrom1
        self.gene1Name = gene1Name # someting like BRCA1
        self.breakpointLoc1 = breakpointLoc1
        self.exonNum1 = exonNum1 # exon in which (or before which if intronic/ intergenic) breakpoint occurs        
        
        self.transcript2ID = transcript2ID
        self.ensGeneID2 = ensGeneID2
        self.geneStrand2 = geneStrand2
        self.transcript2GenomicBreakPos = transcript2GenomicBreakPos # same for transcript 2
        self.chrom2 = chrom2
        self.gene2Name = gene2Name
        self.breakpointLoc2 = breakpointLoc2
        self.exonNum2 = exonNum2 # exon in which (or before which if intronic/ intergenic) breakpoint occurs        
        
        self.genePairNumber = genePairNumber # index of gene in which fusion discovered 
        self.geneTranscriptPairNumber = geneTranscriptPairNumber # index of gene-specific transcript combination 
        self.totalNumberOfTranscriptPairs = totalNumberOfTranscriptPairs
        
        self.orientation = orientation
        
        self.customDownstream = customDownstream
        
    def reciprocal(self):
        '''
        Makes and returns a fusion that is the reciprocal of the current fusion. Must add one to the genomic breakpoint position for the 
        new downstream gene due to how indexing occurs
        '''
        return Fusion(self.ensGeneID2, self.geneStrand2, self.transcript2ID, self.chrom2, self.transcript2GenomicBreakPos -1 if self.geneStrand2 == "+" else self.transcript2GenomicBreakPos +1, \
                      self.ensGeneID1, self.geneStrand1, self.transcript1ID, self.chrom1, self.transcript1GenomicBreakPos +1 if self.geneStrand1 == "+" else self.transcript1GenomicBreakPos -1, \
                      genePairNumber = self.genePairNumber, gene1Name = self.gene2Name, gene2Name = self.gene1Name, breakpointLoc1 = self.breakpointLoc2, breakpointLoc2= self.breakpointLoc1, \
                      exonNum1 = self.exonNum2, exonNum2 = self.exonNum1, geneTranscriptPairNumber = self.geneTranscriptPairNumber,\
                       totalNumberOfTranscriptPairs= self.totalNumberOfTranscriptPairs, orientation = self.orientation)    
    
    def __str__(self):
        return "[Fusion: %s(%s) (%s): %s (%s:%u)|%s(%s) (%s): %s (%s:%u)]" %(self.ensGeneID1, self.gene1Name, self.geneStrand1, \
                                                                             self.transcript1ID, self.chrom1, self.transcript1GenomicBreakPos, \
                                                           self.ensGeneID2, self.gene2Name, self.geneStrand2, \
                                                           self.transcript2ID, self.chrom2, self.transcript2GenomicBreakPos)
    
    def __repr__(self):
        return self.__str__()
 
class FusionOutputParser(object):
    '''
    Abstract class for all parsers of fusion-finding tool output
    '''
    
    def __init__(self, geneToTranscriptIDToExonList, geneToTranscriptIDToStartEnd, useCustomDownstreamSequence):
        '''
        Input transcriptome annotation information
        '''
        self.geneToTranscriptIDToExonList = geneToTranscriptIDToExonList
        self.geneToTranscriptIDToStartEnd = geneToTranscriptIDToStartEnd
        self.skippedDict = {"transcriptNotFound":0, "transcriptsWritten":0, "breakUpstreamOrDownstream":0, "inconsistentDirectionality":0, \
                            "geneNotInAnnotation":0, "noSpanningTranscripts":0, "geneFusionJunctionsimportedSuccessfully":0, "totalFusionJunctions":0}
        
        self.useCustomDownstreamSequence = useCustomDownstreamSequence
        
        
    
    def parseOutput(self, outF, verbose):
        '''
        Parses output a list of Fusion events
        '''
        raise NotImplementedError("Use a subclass")
    
    def _getTranscriptIDsWithIncludedBreak(self, geneID, genomicBreakPosition, transcriptomicLocation='exonic'):
        '''
        Given a geneID , find all transcripts that traverse the genomic break
        '''
        ids = []
        try:
            transcriptToStartEndDict = self.geneToTranscriptIDToStartEnd[geneID] # this gene is not in the annotation
        except KeyError:
            return geneID
        for (transcriptID, (start,end)) in transcriptToStartEndDict.items():
            if (start < genomicBreakPosition <= end): # it's within this transcript
                if transcriptomicLocation != 'intron': 
                    ids += [transcriptID]
                else: # if this genomic breakpoint has been marked as an intronic, only take transcripts for which this genomic location is intronic
                    exonic = False 
                    for (exonStart, exonEnd) in self.geneToTranscriptIDToExonList[geneID][transcriptID]:
                        if exonStart < genomicBreakPosition <= exonEnd: # this location is actually exonic for this transcript
                            exonic = True
                            break
                    if not exonic: # this genomic breakpoint is indeed intronic with respect to this transcript
                        ids += [transcriptID] 
        return ids    

class DeFuseFusionOutputParser(FusionOutputParser):
    '''
    Parses output from deFuse fusion finder
    '''
            
    def parseOutput(self, outF, verbose):
        '''
        Parses output file into list of Fusion objects (which consist of transcripts with relative breakpoints). 
        '''
        fusionList = []
        reader = csv.DictReader(outF, delimiter = '\t')        
        needFullGenome = False
        
        # Check that reading has happened successfully
        for (genePairNumber, row) in enumerate(reader):
            try:
                row['gene_location1'] 
            except KeyError:
                sys.stderr.write("Error reading fusion junction file {0}; check if the file is properly formed with correct header".format(outF))
                sys.exit(1)
            
            # Ignore this fusion if either breakpoint is upstream or downstream of the actual gene
            if ("upstream" in row['gene_location1'] or "downstream" in row['gene_location1']) or ((not self.useCustomDownstreamSequence) and ("upstream" in row['gene_location2'] or "downstream" in row['gene_location2'])):
                if verbose:                
                    sys.stderr.write( "Warning: skipping %s|%s fusion because breakpoint is upstream or downstream of one of the two constituent genes\n" %(row['gene1'], row['gene2']))
                try: 
                    self.skippedDict["breakUpstreamOrDownstream"] +=1
                except KeyError:
                    self.skippedDict["breakUpstreamOrDownstream"] = 1                
                continue            
            
            # Ignore this fusion if directionality is not consistent with promoter usage (fusion transcript produced such that promoter was somehow reversed in directionality).            
            if (row['gene_align_strand1'] != '+') or ((not self.useCustomDownstreamSequence) and row['gene_align_strand2'] != '-'): # might want to look into this case
                if verbose:
                    sys.stderr.write( "Warning: skipping %s|%s fusion because directionality of implied fusion genes inconsistent with promoter and strand directionality\n" %(row['gene1'], row['gene2']))
                try:
                    self.skippedDict["inconsistentDirectionality"] +=1
                except KeyError:
                    self.skippedDict["inconsistentDirectionality"] = 1
                
                continue
            
            # If there are intronic fusions, we need the full genome.
            if "intron" in (row['gene_location1']) and ((not self.useCustomDownstreamSequence) and  "intron" in row['gene_location2']): 
                needFullGenome = True
            
            # Get transcript IDs of traversed breakpoints 
            (transcriptIDs1, transcriptIDs2) = (self._getTranscriptIDsWithIncludedBreak(row['gene1'], int(row['genomic_break_pos1']), row['gene_location1']), \
                                                self._getTranscriptIDsWithIncludedBreak(row['gene2'], int(row['genomic_break_pos2']), row['gene_location2']) if (not self.useCustomDownstreamSequence) else ['customDownstraem'])
            
            if type(transcriptIDs1) == str or type(transcriptIDs2) == str: # at least one of the two constituent genes were not in the input annotation (expect a list)
                if verbose:
                    if type(transcriptIDs1) == str:
                        sys.stderr.write("Warning: gene {0}, which was identified as part of a fusion by the fusion list software, is not in the input transcriptome .annotation file\n".format(transcriptIDs1))
                    if type(transcriptIDs2) == str:
                        sys.stderr.write("Warning: gene {0}, which was identified as part of a fusion by the fusion list software, is not in the input transcriptome .annotation file\n".format(transcriptIDs2))
                
                try:
                    self.skippedDict["geneNotInAnnotation"] +=1
                except KeyError:
                    self.skippedDict["geneNotInAnnotation"] = 1
                continue
                    
            
            if (len(transcriptIDs1) == 0) or (len(transcriptIDs2) == 0): # at least one of the two genes had no input transcripts
                if verbose:
                    sys.stderr.write( "Warning: skipping %s|%s fusion because unable to find transcripts in one or both genes spanning the genomic fusion location\n" %(row['gene1'], row['gene2']))
                
                try:
                    self.skippedDict["noSpanningTranscripts"] +=1
                except KeyError:
                    self.skippedDict["noSpanningTranscripts"] =1 
                continue 
            
            try:
                self.skippedDict["geneFusionJunctionsimportedSuccessfully"] +=1
            except KeyError:
                self.skippedDict["geneFusionJunctionsimportedSuccessfully"] = 1
                
            
            geneTranscriptPairNumber = 0
            # Make every combination of fusion transcripts implied by gene
            for tsID1 in transcriptIDs1:
                for tsID2 in transcriptIDs2:
                    geneTranscriptPairNumber +=1                
                    fusionList += [Fusion(row['gene1'], row['gene_strand1'], tsID1, 'chr' + row['gene_chromosome1'], int(row['genomic_break_pos1']),\
                                           row['gene2'], row['gene_strand2'], tsID2, 'chr' + row['gene_chromosome2'], int(row['genomic_break_pos2']),\
                                           gene1Name= row['gene_name1'], gene2Name = row['gene_name2'],\
                                           breakpointLoc1 = row['gene_location1'], breakpointLoc2=row['gene_location2'],\
                                            genePairNumber = genePairNumber +1, geneTranscriptPairNumber = geneTranscriptPairNumber, \
                                            totalNumberOfTranscriptPairs = len(transcriptIDs1) * len(transcriptIDs2), customDownstream= self.useCustomDownstreamSequence)]

                    
        
        self.skippedDict["totalFusionJunctions"] = genePairNumber + 1        
        return (fusionList, self.skippedDict, needFullGenome)
            
class PRADAFusionOutputParser(FusionOutputParser):
    
    def __init__(self, geneToTranscriptIDToExonList, geneToTranscriptIDToStartEnd, HGNCNametoENSName, useCustomDownstream):
        super(PRADAFusionOutputParser, self).__init__(geneToTranscriptIDToExonList, geneToTranscriptIDToStartEnd, useCustomDownstream)
        self.HGNCNametoENSName = HGNCNametoENSName
    
    def parseOutput(self, outF, verbose):
        fusionList = []
        reader = csv.DictReader(outF, delimiter ="\t")
        for (genePairNumber, row) in enumerate(reader):
            
            # first step: get Ensembl gene name of both genes
            try:
                ensGeneID1 = self.HGNCNametoENSName[row['Gene_A']]
                ensGeneID2 = self.HGNCNametoENSName[row['Gene_B']]
            except KeyError:                                
                try:
                    self.skippedDict["geneNotInAnnotation"] +=1
                except KeyError:
                    self.skippedDict["geneNotInAnnotation"] = 1                
                if verbose:
                    sys.stderr.write("Warning: one of genes of name {0} or {1}, which were identified as part of a fusion by the fusion list software, is not the input transcriptome GTF annotation\n".format(row['Gene_A'], row['Gene_B']))
                continue
            
            
            # second step: get transcript IDs traversing breakpoints            
            gene1Breakpoint = int(row['Junction'].split("_")[0].split(":")[-1])
            gene2Breakpoint = int(row['Junction'].split("_")[1].split(":")[-1].split(",")[0])
            
            (transcriptIDs1, transcriptIDs2) = (self._getTranscriptIDsWithIncludedBreak(ensGeneID1, gene1Breakpoint), self._getTranscriptIDsWithIncludedBreak(ensGeneID2, gene2Breakpoint))            
            if type(transcriptIDs1) == str or type(transcriptIDs2) == str: # at least one of the two constituent genes were not in the input annotation
                if verbose:
                    if type(transcriptIDs1) == str:
                        sys.stderr.write("Warning: gene {0}, which was identified as part of a fusion by the fusion list software, is not the input transcriptome annotation\n".format(row['Gene_A']))
                    if type(transcriptIDs2) == str:
                        sys.stderr.write("Warning: gene {0}, which was identified as part of a fusion by the fusion list software, is not the input transcriptome annotation\n".format(row['Gene_B']))                
                try:
                    self.skippedDict["geneNotInAnnotation"] +=1
                except KeyError:
                    self.skippedDict["geneNotInAnnotation"] = 1
                continue
            
            if (len(transcriptIDs1) == 0) or (len(transcriptIDs2) == 0): # at least one of the two genes had no input transcripts
                if verbose:
                    sys.stderr.write( "Warning: skipping %s|%s fusion because unable to find transcripts in one or both genes spanning the genomic fusion location\n" %(row['Gene_A'], row['Gene_B']))
                
                try:
                    self.skippedDict["noSpanningTranscripts"] +=1
                except KeyError:
                    self.skippedDict["noSpanningTranscripts"] =1 
                continue 
            
            try:
                self.skippedDict["geneFusionJunctionsimportedSuccessfully"] +=1
            except KeyError:
                self.skippedDict["geneFusionJunctionsimportedSuccessfully"] = 1
            
            # third step: make combinatorial combination of all transcript pairs 
            geneTranscriptPairNumber = 0
            # Make every combination of fusion transcripts implied by gene
            for tsID1 in transcriptIDs1:
                for tsID2 in transcriptIDs2:
                    geneTranscriptPairNumber +=1                    
                    fusionList += [Fusion(ensGeneID1, "+" if int(row['A_strand']) == 1 else "-", tsID1, 'chr' + row['A_chr'], gene1Breakpoint,\
                                           ensGeneID2, "+" if int(row['B_strand']) == 1 else "-", tsID2, 'chr' + row['B_chr'], gene2Breakpoint,\
                                            gene1Name = row['Gene_A'], gene2Name=row['Gene_B'], genePairNumber = genePairNumber +1, \
                                            geneTranscriptPairNumber = geneTranscriptPairNumber, totalNumberOfTranscriptPairs = len(transcriptIDs1) * len(transcriptIDs2))]                
        
        self.skippedDict["totalFusionJunctions"] = genePairNumber + 1        
        return (fusionList, self.skippedDict, False)                        
            

def main():
    ## Parse command line arguments ##
    parser = argparse.ArgumentParser(description = "Make fusion transcripts from transcriptome file and fusion list")
    # Required arguments
    parser.add_argument('transcriptomeFile', type = file, \
                        help = "transcriptome (from UCSC, Ensembl, RefSeq, etc.) fasta file with transcript name headers in UCSC table downloads (http://genome.ucsc.edu/cgi-bin/hgTables) format. Include all exons, including those in UTRs")    
    
    parser.add_argument('transcriptomeAnnotationFile', type = file, \
                        help  = "transcriptome (from UCSC, Ensembl, RefSeq, etc.) annotation file with columns as in UCSC table downloads (http://genome.ucsc.edu/cgi-bin/hgTables) format (headers: \n bin    name    chrom    strand    txStart    txEnd    cdsStart    cdsEnd    exonCount    exonStarts    exonEnds    score    name2    cdsStartStat    cdsEndStat    exonFrames)")
    
    parser.add_argument('fusionListFile', type = file, help = "fusion output list file")
    
    # Optional arguments    
    parser.add_argument('--transcriptomeAnnotationFormat', type=str, \
                        help = "transcriptome annotation format. Only Ensembl supported so far", default = 'Ensembl')
        
    parser.add_argument('--upstreamFusionSoftware', type = str, default= "deFuse", help = "type of fusion output list; current options are 'deFuse, PRADA'")
    parser.add_argument('--GTFFile', type=file, default = None, help= "GTF annotation file. Required for PRADA output in order to converge HGNC gene names to Ensembl gene names")
    parser.add_argument('-g', '--twoBitGenomeFile', default=None, type=file,\
                         help="2bit genome sequence file from UCSC (http://hgdownload.cse.ucsc.edu/goldenPath/hg19/bigZips/hg19.2bit) for use in constructing candidate fusion transcripts with intronic and intergenic portions")    
    parser.add_argument('-o', '--output', default = None, help= "File to which to output updated transcriptome")
    
    parser.add_argument('-v', '--verbose', default = False, action='store_true', help="Verbose mode: output specific fusion transcripts skipped, etc.")
    parser.add_argument('-r', '--includeReciprocals', type=str, default = "", nargs='*', help="For the specified gene name(s), create reciprocal fusions for these genes in this sample (switching upstream and downstream gene).")
    
    parser.add_argument('-t', '--histograms', default = None, type=argparse.FileType('w'), help= "Write transcript pairs per gene pair (gene-resolution fusion junction) histogram to the following file")
    parser.add_argument('-f', '--focusTranscriptsFile', default=None, type=argparse.FileType('w'), help="Write fasta file that consists of transcripts to focus on (the fusion transcripts and their constituent transcripts)")
    
    parser.add_argument('-d', '--customDownstreamSequenceFile', default=None, type = file, help="Custom sequence, provided in .fa file, to use in lieu of downstream transcript for each fusion transcript")



    namespace = parser.parse_args()
    if namespace.upstreamFusionSoftware == 'PRADA' and namespace.GTFFile == None:
        parser.error("Error: if PRADA upstreamFusionSoftware is used, GTF File must be specified (with parameter --GTFFile)")
    
    
    # create fusion output parser object    
    ftm = FusionTranscriptMaker(namespace.transcriptomeFile, namespace.transcriptomeAnnotationFile, \
                                namespace.transcriptomeAnnotationFormat, namespace.upstreamFusionSoftware, namespace.histograms, namespace.verbose, \
                                namespace.focusTranscriptsFile, namespace.GTFFile, namespace.twoBitGenomeFile, namespace.includeReciprocals, \
                                namespace.customDownstreamSequenceFile)    
    
    # Update transcriptome with transcripts from fusion list file, and send to output
    ftm.updateTranscriptome(namespace.fusionListFile, namespace.output)            

if __name__ == '__main__':
    main()
    
    
        