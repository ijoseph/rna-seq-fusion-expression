'''
Created on Mar 8, 2013

@author: Isaac Joseph 

Unit tests for FusionTranscriptMaker correctness.

'''
from FusionTranscriptMaker import Fusion, FusionTranscriptMaker
import sys
import unittest
import re



    

class Test(unittest.TestCase):


    def setUp(self):
        #   pos:  123456789012345
        # + chr1: 11111-----22222 (where 1 and 2 are bases in exons 1 and 2, respectively, of transcript "one" (the only transcript of gene "one-gene"))
        # - chr2: BBBBB-----AAAAA (where A and B are bases in exons 1 and 2, respectively, of transcript "two" (the only transcript of gene "two-gene"))
        # + chr3: 88888-----99999
        # - chr4: YYYYY-----XXXXX+
         
        transFA = open('test-transcriptome.fa')
        transAnn = open('test-transcriptome.annotation')
        
        # Set up makers with various configurations of including or not including reciprocals        
        
        self.ftmAllReciprocals = FusionTranscriptMaker(transFA, transAnn, "Ensembl", "deFuse", None, True, None, None, None, ["all"], None) 
        self.ftmOneReciprocal = FusionTranscriptMaker(transFA, transAnn, "Ensembl", "deFuse", None, True, None, None, None, ["one-gene"], None)  
        self.ftmOneThreeReciprocal = FusionTranscriptMaker(transFA, transAnn, "Ensembl", "deFuse", None, True, None, None, None, ["one-gene|three-gene"],None)                     
        
        # one-gene
        self.plusGene1GenomicBreakPositions={'intronic': 8, 'exonic': 3, 'exon_begin_border': 10, 'exon_end_border': 5}        
        self.plusGene1RetainedSequences={'intronic': '11111', 'exonic': '111', 'exon_begin_border': '11111', 'exon_end_border': '11111'}
        self.plusGene1GenomicCoordinates={'intronic': (1,8), 'exonic': (1,3), 'exon_begin_border': (1,10), 'exon_end_border': (1,5)}
        # Reciprocals
        self.recipPlusGene1RetainedSequences= {'intronic': '22222', 'exonic': '1122222', 'exon_begin_border': '22222', 'exon_end_border': '22222'} # these are actually the downstream sequences
        self.recipPlusGene1GenomicCoordinates = {'intronic': (9,15), 'exonic': (4,15), 'exon_begin_border': (11,15), 'exon_end_border': (6,15)}
        
        # three-gene
        self.plusGene2GenomicBreakPositions={'intronic': 9, 'exonic': 4, 'exon_begin_border': 11, 'exon_end_border': 6}
        self.plusGene2RetainedSequences={'intronic': '99999', 'exonic': '8899999', 'exon_begin_border': '99999', 'exon_end_border': '99999'}
        self.plusGene2GenomicCoordinates={'intronic': (9,15), 'exonic': (4,15), 'exon_begin_border': (11,15), 'exon_end_border': (6,15)}
        # Reciprocals
        self.recipPlusGene2RetainedSequences = {'intronic': '88888', 'exonic': '888', 'exon_begin_border': '88888', 'exon_end_border': '88888'} # actually the upstream sequences
        self.recipPlusGene2GenomicCoordinates = {'intronic': (1,8), 'exonic': (1,3), 'exon_begin_border': (1,10), 'exon_end_border': (1,5)}
        
                
        # two-gene
        self.minusGene1GenomicBreakPositions={'intronic': 8, 'exonic': 13, 'exon_begin_border': 6, 'exon_end_border': 11}
        self.minusGene1RetainedSequences={'intronic': 'AAAAA', 'exonic': 'AAA', 'exon_begin_border': 'AAAAA', 'exon_end_border': 'AAAAA'}
        self.minusGene1GenomicCoordinates={'intronic': (8,15), 'exonic': (13,15), 'exon_begin_border': (6,15), 'exon_end_border': (11,15)}
        # Reciprocals
        self.recipMinusGene1RetainedSequences={'intronic': 'BBBBB', 'exonic': 'AABBBBB', 'exon_begin_border': 'BBBBB', 'exon_end_border': 'BBBBB'} # the downstream portion is retained
        self.recipMinusGene1GenomicCoordinates = {'intronic': (1,7), 'exonic': (1,12), 'exon_begin_border': (1,5), 'exon_end_border': (1,10)}
        
        # four-gene
        self.minusGene2GenomicBreakPositions={'intronic': 7, 'exonic': 12, 'exon_begin_border': 5, 'exon_end_border': 10}
        self.minusGene2RetainedSequences={'intronic': 'YYYYY', 'exonic': 'XXYYYYY', 'exon_begin_border': 'YYYYY', 'exon_end_border': 'YYYYY'}
        self.minusGene2GenomicCoordinates={'intronic': (1,7), 'exonic': (1,12), 'exon_begin_border': (1,5), 'exon_end_border': (1,10)}                
        # Reciprocals
        self.recipMinusGene2RetainedSequences={'intronic': 'XXXXX', 'exonic': 'XXX', 'exon_begin_border': 'XXXXX', 'exon_end_border': 'XXXXX'}
        self.recipMinusGene2GenomicCoordinates={'intronic': (8,15), 'exonic': (13,15), 'exon_begin_border': (6,15), 'exon_end_border': (11,15)}        
                
        self.tempOutput = 'temp-fusion-transcripts.fa'
        
        self.customDownstreamFile = 'sample-custom-downstream.fa'
        
        # Set up genes for reciprocals        

         

    def tearDown(self):
        pass
    
    def _validateCreation(self,gene1GenomicBreakPositions, gene2GenomicBreakPositions, gene1RetianedSequences, \
                          gene2RetainedSequences, gene1Name, gene2Name, ts1Name, ts2Name, gene1Orientation, gene2Orientation, \
                          gene1Coordinates, gene2Coordinates, recipGene1RetainedSequences, recipGene2RetainedSequences, recipGene1Coordinates, recipGene2Coordinates):
        '''
        Does the work of validating the creation of every possible combinatorial combination of fusion breakpoint locations by constructing each location combination
        with FusionTranscriptMaker and then comparing the created fusion transcripts to the predicted retained sequence. 
        '''
        testNumber = 1                 
        for (location1, breakPosition1) in gene1GenomicBreakPositions.items():
            for (location2, breakPosition2) in gene2GenomicBreakPositions.items():
                fusionObjList = [Fusion(gene1Name, gene1Orientation, ts1Name, "chrTest", breakPosition1, gene2Name, gene2Orientation, ts2Name, "chrTest", breakPosition2, gene1Name = gene1Name, gene2Name = gene2Name)]                
                
                self.ftmAllReciprocals.updateTranscriptome(fusionObjList, self.tempOutput)        
                discoveredFusionSequence = self._getCreatedFusionTranscriptSequence(self.tempOutput)[0]        
                
                #
                # (1) Validate that some fusion sequence was created
                #
                self.assertIsNotNone(discoveredFusionSequence, "No fusion sequence parsed for some reason")                
                
                #
                # (2) Validate that discovered fusion sequence was as expected
                #
                predictedSequence = gene1RetianedSequences[location1] + gene2RetainedSequences[location2] 
                self.assertEqual(discoveredFusionSequence, predictedSequence , \
                             "[Test %u]: Fusion transcript is not the same as expected for \norientation:\n %s %s, \nposition:\n %s %s; \nexpected vs. acquired: \n%s\n%s\n\nbreak positions:\ntranscript 1: %s at %u; transcript 2: %s at %u " \
                             %(testNumber, gene1Orientation, gene2Orientation, location1, location2, \
                               predictedSequence, discoveredFusionSequence, gene1Name, gene1GenomicBreakPositions[location1], \
                               gene2Name, gene2GenomicBreakPositions[location2] ))                
                #
                # (3) Validate that the reported chromosomal coordinates of the fusion transcript are correct
                #
                headerInfo = self._parseFusionTranscriptHeaders(self.tempOutput)[0]                
                self.assertEqual(headerInfo[1][0], gene1Coordinates[location1], \
                                 "genomic coordinates of created fusion for gene 1 are incorrect. Expected {0}, got {1} "\
                                 .format(gene1Coordinates, headerInfo[1][0]))
                self.assertEqual(headerInfo[1][1], gene2Coordinates[location2], \
                                 "genomic coordinates of created fusion for gene 2 are incorrect. Expected {0}, got {1} "\
                                 .format(gene2Coordinates, headerInfo[1][1]))
                
                #
                # (4) Validate some reciprocal fusion sequence was created (if called for)
                # 
                if self.ftmAllReciprocals.includeReciprocals:
                    recipDiscoveredFusionSequence = self._getCreatedFusionTranscriptSequence(self.tempOutput)[1]
                
                    #
                    # (1') Validate that some reciprocal fusion sequence was created
                    #
                    self.assertIsNotNone(recipDiscoveredFusionSequence, "No reciprocal fusion sequence parsed")
                    
                       
                    #
                    # (2') Validate that reciprocal discovered fusion sequence was as expected
                    # 
                    recipPredictedSequence = recipGene2RetainedSequences[location2] + recipGene1RetainedSequences[location1]
#                     print("Predicted fusion transcript:\n{0}\nPredicted reciprocal fusion transcript:\n{1}\nbreakpoint locatoins:\n{2},{3}, total length {4}"\
#                           .format(predictedSequence, \
#                                   recipPredictedSequence, breakPosition1, \
#                                   breakPosition2, len(predictedSequence) + len(recipPredictedSequence)))
                    
                    # Does our prediction satisfy conservation of bases? 
                    self.assertEqual(len(predictedSequence + recipPredictedSequence), 20, \
                                     "[Test {2}]: We've violated conservation of bases with our predicted / predicted reciprocal fusion transcripts. regular:\n{0}\nreciprocal\n{1}\n. Fusions are \n{3}\n{4}"\
                                     .format(predictedSequence, recipPredictedSequence, testNumber, fusionObjList[0], fusionObjList[-1]))
                    
                    # Do our actual created sequences satisfy conservation of bases?                                        
                    self.assertEqual(len(discoveredFusionSequence + recipDiscoveredFusionSequence), 20, \
                                     "[Test {2}]: We've violated conservation of bases with our predicted / predicted reciprocal fusion transcripts. regular:\n{0}\nreciprocal\n{1}\n Fusions are \n{3}\n{4}"\
                                     .format(predictedSequence, recipPredictedSequence, testNumber, fusionObjList[0], fusionObjList[-1]))
                    
                    self.assertEqual(recipDiscoveredFusionSequence, recipPredictedSequence, \
                                     """[Test {0}]: Fusion transcript is not the same as expected for 
                                     orientation {1} {2},
                                     position {3} {4},
                                     expected vs. acquired:
                                     {5}
                                     {6}
                                     break positions: 
                                     transcript 1: {7} at {8}
                                     transcript 2: {9} at {10}                                    
                                     """.format(testNumber, \
                                                gene1Orientation, gene2Orientation, \
                                                location1, location2,\
                                                recipPredictedSequence, \
                                                recipDiscoveredFusionSequence, \
                                                 gene1Name, gene1GenomicBreakPositions[location1], \
                                                 gene2Name, gene2GenomicBreakPositions[location2]))

                    # 
                    # (3') Validate that the reported discovered chromosomal coordinates of the fusion transcript are correct
                    # 
                    
                    recipHeaderInfo = self._parseFusionTranscriptHeaders(self.tempOutput)[1]
                     
                    self.assertEqual(recipHeaderInfo[1][0], recipGene2Coordinates[location2], \
                                 "[Test {6}]: genomic coordinates of created fusion for reciprocal gene 2 are incorrect. Expected {0}, got {1}\n(locations:\n{2}:{3},{4}:{5})"\
                                 .format(recipGene2Coordinates[location2], recipHeaderInfo[1][0], location1, breakPosition1, location2, breakPosition2, testNumber))
                     
                    self.assertEqual(recipHeaderInfo[1][1], recipGene1Coordinates[location1], \
                                 "[Test {6}]: genomic coordinates of created fusion for reciprocal gene 1 are incorrect. Expected {0}, got {1}\n(locations:\n{2}:{3},{4}:{5})"\
                                 .format(recipGene1Coordinates[location1], recipHeaderInfo[1][1], location1, breakPosition1, location2, breakPosition2, testNumber))
                     
                    
                     

                testNumber +=1    
                
    def test1PlusPlus(self):
        '''
        Tests proper fusion gene creation from two genes that are both on the positive genomic strand.  '''
        
        self._validateCreation(self.plusGene1GenomicBreakPositions, self.plusGene2GenomicBreakPositions, self.plusGene1RetainedSequences,\
                                self.plusGene2RetainedSequences, 'one-gene', 'three-gene', 'one', 'three', '+', '+', \
                                self.plusGene1GenomicCoordinates, self.plusGene2GenomicCoordinates, \
                                self.recipPlusGene1RetainedSequences, self.recipPlusGene2RetainedSequences, \
                                self.recipPlusGene1GenomicCoordinates, self.recipPlusGene2GenomicCoordinates)
        
    def test2PlusMinus(self):
        '''
        Tests proper fusion gene creation from two genes that are both on the positive genomic strand.  '''
        
        self._validateCreation(self.plusGene1GenomicBreakPositions, self.minusGene2GenomicBreakPositions, self.plusGene1RetainedSequences, \
                               self.minusGene2RetainedSequences, 'one-gene', 'four-gene', 'one', 'four', '+', '-', \
                               self.plusGene1GenomicCoordinates, self.minusGene2GenomicCoordinates, \
                               self.recipPlusGene1RetainedSequences, self.recipMinusGene2RetainedSequences, \
                               self.recipPlusGene1GenomicCoordinates, self.recipMinusGene2GenomicCoordinates)

    def test3MinusPlus(self):    
        '''
        Tests proper fusion gene creation from two genes that are both on the positive genomic strand.  '''
        
        self._validateCreation(self.minusGene1GenomicBreakPositions, self.plusGene2GenomicBreakPositions, self.minusGene1RetainedSequences, \
                               self.plusGene2RetainedSequences, 'two-gene', 'three-gene', 'two', 'three', '-', '+', \
                               self.minusGene1GenomicCoordinates, self.plusGene2GenomicCoordinates, \
                               self.recipMinusGene1RetainedSequences, self.recipPlusGene2RetainedSequences, \
                               self.recipMinusGene1GenomicCoordinates, self.recipPlusGene2GenomicCoordinates)
    
    def test4MinusMinus(self):
        
        self._validateCreation(self.minusGene1GenomicBreakPositions, self.minusGene2GenomicBreakPositions, self.minusGene1RetainedSequences, \
                               self.minusGene2RetainedSequences, 'two-gene', 'four-gene', 'two', 'four', '-', '-', \
                               self.minusGene1GenomicCoordinates, self.minusGene2GenomicCoordinates, \
                               self.recipMinusGene1RetainedSequences, self.recipMinusGene2RetainedSequences, \
                               self.recipMinusGene1GenomicCoordinates, self.recipMinusGene2GenomicCoordinates)

    def testReciprocals(self):
        '''
        Test to see that reciprocals are correctly created
        '''
        
        # Test 1: specify that all fusions involving the gene 'one' should be made reciprocal         
        fusionObjList = [Fusion("one-gene", "+", "one", "chrTest", 5, "three-gene", "+", "three", "chrTest", 5, gene1Name = "one-gene", gene2Name = "three-gene")]
        self.ftmOneReciprocal.updateTranscriptome(fusionObjList, self.tempOutput)
        output = self._getCreatedFusionTranscriptSequence(self.tempOutput)
        self.assertEqual(len(output), 2, "did not successfully create reciprocal for 'one' gene")
        
        # Test 2: specify by fusions for reciprocal creation by both upstream and downstream ('one|three')
        fusionObjList = [Fusion("one-gene", "+", "one", "chrTest", 5, "three-gene", "+", "three", "chrTest", 5, gene1Name = "one-gene", gene2Name = "three-gene")]
        self.ftmOneThreeReciprocal.updateTranscriptome(fusionObjList, self.tempOutput)
        output = self._getCreatedFusionTranscriptSequence(self.tempOutput)
        self.assertEqual(len(output), 2, "did not successfully create reciprocal for 'one|three' fusion")        
        
        # Test 3: only specified fusion is repeated
        fusionObjList =  [Fusion("one-gene", "+", "one", "chrTest", 5, "three-gene", "+", "three", "chrTest", 5, gene1Name = "one-gene", gene2Name = "three-gene")] + \
                        [Fusion("two-gene", "+", "two", "chrTest", 5, "four-gene", "+", "four", "chrTest", 5, gene1Name = "two-gene", gene2Name = "four-gene")]
                        
        self.ftmOneThreeReciprocal.updateTranscriptome(fusionObjList, self.tempOutput)
        output= self._getCreatedFusionTranscriptSequence(self.tempOutput)
        self.assertEqual(len(output), 3, "did not correctly specify that reciprocal should be only created for one fusion")
        
        # Test no reciprocals created        
        
    def testCustom(self):
        '''
        Tests that custom downstream transcript is correctly added
        '''
    
    
    def _getCreatedFusionTranscriptSequence(self, filename):
        '''
        Gets sequence from fusion transcript list
        '''
        
        f = open(filename)
        fusionSeqList = []
        seq = ''
        for line in f:
            if line.startswith('>'):                
                if len(seq) > 0:
                    fusionSeqList += [seq]
                seq = ''
            else:
                seq += line.strip()
        
        f.close()
        fusionSeqList += [seq]
        return fusionSeqList
    
    def _parseFusionTranscriptHeaders(self, filename):
        '''
        Parses the created fusion transcript's header and extracts the genomic coordinates of the fusion transcript (and also the 
        relative break position)
        '''
        headerInfo = [] # (rel break pos, ((upstream chrom start, chrom end), (downstream chrom start, downstream chrom end)))
        with open(filename) as f:
            for line in f:
                if not line.startswith('>'):
                    continue
                lineAr = line.split()
                try:
                    relBreakPos = int(re.search(r'\d+$', lineAr[0]).group(0))
                    
                    upstreamChromosomalStart = int(re.findall(r':(\d+)', lineAr[1])[0])
                    upstreamChromosomalEnd = int(re.findall(r'-(\d+)', lineAr[1])[0])
                    
                    downstreamChromosomalStart = int(re.findall(r':(\d+)', lineAr[1])[1])
                    downstreamChromosomalEnd = int(re.findall(r'-(\d+)', lineAr[1])[1])                    
                    
                except ValueError:
                    sys.stderr.write("""Error parsing fusion transcript header; not in expected format
                    expected something in the form of:
                    >hg19_ensGene_one|three_deFuse-gene-pair-000001_(common_name_1|common_name_2):transcript-pair-000001_transcript-relative-break-position-5 range=chr1:1-5|chr3:6-15 5'pad=0 3'pad=0 strand=+|+ repeatMasking=none
                    """)
                    
                    sys.stderr.write("Actually received:\n" + line)
                    return
                
                headerInfo += [(relBreakPos, ((upstreamChromosomalStart,  upstreamChromosomalEnd), (downstreamChromosomalStart, downstreamChromosomalEnd)))]

        return headerInfo
                    
                    
                
                
                             
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
