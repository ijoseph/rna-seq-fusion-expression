'''
Created on Nov 5, 2013

@author: ijoseph

Pipeline that runs TCGA-based fusion transcript expression pipeline, taking in fusion junction file and cghub manifest XML file, finding
RNA-seq reads, pulling out relevant fusion junction calls from the fusion junction file, and creating fusion transcript expression estimates. 
'''
from fabric.api import settings, hide
from fabric.operations import local
from transcriptmake.FusionTranscriptMaker import *
import StringIO, argparse, multiprocessing, os, sys
import xml.etree.ElementTree as ET
from ruffus import * 


# Parse command-line arguments
parser = argparse.ArgumentParser(description = """ Runs TCGA-based fusion transcript expression pipeline, taking in fusion junction file and cghub manifest XML file, finding
RNA-seq reads, pulling out relevant fusion junction calls from the fusion junction file, and creating fusion transcript expression estimates. """) 
parser.add_argument("fusionListFile", type=file, help="fusion output list file, from deFuse or PRADA")
parser.add_argument("cancerGenomicsHubXMLFile", type=file, help="cancer genomics hub manifest.xml file")
parser.add_argument("downloadRoot", help="folder whose subdirectories are analysis_ids corresponding to specific samples/patients")
parser.add_argument('transcriptomeFASTA', type = str, \
                        help = """transcriptome (from UCSC, Ensembl, RefSeq, etc.) fasta file with transcript name headers in UCSC table downloads 
                        (http://genome.ucsc.edu/cgi-bin/hgTables) format. Include all exons, including those in UTRs""")
parser.add_argument('transcriptomeAnnotationFile', type = str, \
                        help  = """transcriptome (from UCSC, Ensembl, RefSeq, etc.) annotation file with columns as in UCSC table downloads
                         (http://genome.ucsc.edu/cgi-bin/hgTables) format (headers: 
                         bin    name    chrom    strand    txStart    txEnd    cdsStart    cdsEnd    exonCount    exonStarts    exonEnds    score    name2    cdsStartStat    cdsEndStat    exonFrames)""")
# Optional arguments
parser.add_argument('-p', '--numThreads', type=int, default= multiprocessing.cpu_count()/2, help = "Number of threads to run [(# of CPUs)/2]")
parser.add_argument('--outputFolder', default="Output", help= "folder that will hold output files (expression estimates)")
parser.add_argument('--keepTemporary', type=bool, default=True, help= "keep temporary output files (including .bam)")
parser.add_argument('--transcriptomeAnnotationFormat', type=str, \
                        help = "transcriptome annotation format. Only Ensembl supported so far", default = 'Ensembl')
parser.add_argument('--upstreamFusionSoftware', type = str, default= "deFuse", help = "type of fusion output list; current options are 'deFuse'")
namespace = parser.parse_args()

tempFolder = namespace.outputFolder + os.path.sep + "Pipeline-Temp"


def touchFiles(files):
    if type(files) == list:        
        for f in files:
            if not os.path.exists(os.path.dirname(f)):
                os.makedirs(os.path.dirname(f))
            with file(f, 'a'): # touch the file
                os.utime(f, None)
    else:
        if not os.path.exists(os.path.dirname(files)):
            os.makedirs(os.path.dirname(files))
        with file(files, 'a'):
            os.utime(files, None)


''' Input '''
def initialize(fusionListFile, cancerGenomicsHubXMLFile, downloadRoot):
    '''
    Initializes by parsing cancer genomics XML file to figure out how many samples there are, then getting the 
    associated lines from the fusion list output. 
    '''    
    def parseInput(fusionListFile, cancerGenomicsHubXMLFile, downloadRoot):
        '''
        Takes fusionlistfile and cancerGenomicHubXMLFile and returns list of tuples, each of which represents a single sample. 
        Tuple of format (sampleFusionListFile, sampleOutputFolder) 
        '''
        
        sampleList = []
        
        tree = ET.parse(cancerGenomicsHubXMLFile)        
        for (numSamples, resultSample) in enumerate(tree.iter('Result')): # Loop over samples
            
            # construct the output folder for this sample (used downstream in pipeline for locating genomic read data/ storage of temporary files)
            sampleOutputFolder = (downloadRoot + os.path.sep + resultSample.find('analysis_id').text) 
            
            # Create a pseudo-file (StringIO object) that stores the relevant lines from the 
            # fusion output list file for FusionTranscriptMaker to read downstream 
            header = fusionListFile.readline()
            sampleAliquotID = resultSample.find('aliquot_id').text
            sampleFusionListFile = StringIO.StringIO() 
            sampleFusionListFile.write(header) # start buffer file object as header            
            with settings(hide('everything'), warn_only=True): # don't show any of the grep output                        
                grepResult = local("grep {0} {1}".format(sampleAliquotID, fusionListFile.name), capture=True)
                if grepResult.succeeded:
                    sampleFusionListFile.write(grepResult)
                    sampleFusionListFile.seek(0) # prepare for future reading
                    sampleList += [(sampleFusionListFile, sampleOutputFolder)]
                else:
                    sys.stderr.write("Warning: unable to find any fusions of aliquot_id {0} in fusion list file {1}"\
                                     .format(sampleAliquotID, fusionListFile.name)+ "\nskipping analsyis of this sample\n\n")
                    
        return (sampleList, numSamples + 1)                
        
    (sampleList, numSamples) = parseInput(fusionListFile, cancerGenomicsHubXMLFile, downloadRoot) # parse input files
    sys.stdout.write("{0}/{1} samples in XML file {2} have matching fusions in {3}\n".format(len(sampleList), numSamples, cancerGenomicsHubXMLFile.name, fusionListFile.name))
    return (sampleList, numSamples)


# Initialize pipeline by getting a list of samples
(sampleList, numSamples) = initialize(namespace.fusionListFile, namespace.cancerGenomicsHubXMLFile, namespace.downloadRoot)            

class Test:
    def runFullPipeline(self, sample):
        """
        Runs a full instance of a pipeline on each sample in the sample list
        """        
        ''' Pipeline Tasks'''
        @files(namespace.transcriptomeFASTA, [tempFolder + os.sep + "appended-transcriptome.fa",  tempFolder + os.sep + "focus-transcripts.fa"], \
               namespace.transcriptomeAnnotationFile, namespace.transcriptomeAnnotationFormat, namespace.upstreamFusionSoftware)  
        def fusionTranscriptWrite(transcriptomeFASTA, appendedTranscriptomeAndFocusFiles, transcriptomeAnnotation, transcriptomeAnnotationFormat, upstreamFusionSoftware):    
            '''
            Write fusion transcript-appended transcriptome using fabric
            '''    
            touchFiles(appendedTranscriptomeAndFocusFiles) # remove
            
            
        @transform(fusionTranscriptWrite, suffix(".fa"), ".appended-transcriptome.4.bt2")
        def bowtie2index(updatedTranscriptomeFASTAs, bowtie2TranscriptomeIndex):
            '''
            Index transcript-appended transcriptome
            '''
    #         print updatedTranscriptomeFASTAs, bowtie2TranscriptomeIndex
            
        # Run pipeline    
        def untarReads(sampleList):
            pass
            
        
        pipeline_run(bowtie2index, one_second_per_job = False)

for sample in sampleList:
    t = Test()
    t.runFullPipeline(sample)
