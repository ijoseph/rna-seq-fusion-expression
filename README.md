# README #
Uses called gene fusions in genomic coordinates from [deFuse](https://bitbucket.org/dranew/defuse) or [Prada](http://goo.gl/Cc42T2) to create new transcriptome annotations including possible fusion isoforms. 

Useful for performing fusion expression analysis with [eXpress](bio.math.berkeley.edu/eXpress/) or [Kallisto](http://pachterlab.github.io/kallisto/). More details to follow. 


Run 
```
#!python
python src/transcriptmake/FusionTranscriptMaker.py -h
```
for usage. 

[//]: # ( is this repository for? ###)
[//]: # ()
[//]: # (* Quick summary)
[//]: # (* Version)
[//]: # (* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo))
[//]: # ()
[//]: # (### How do I get set up? ###)
[//]: # ()
[//]: # (* Summary of set up)
[//]: # (* Configuration)
[//]: # (* Dependencies)
[//]: # (* Database configuration)
[//]: # (* How to run tests)
[//]: # (* Deployment instructions)
[//]: # ()
[//]: # (### Contribution guidelines ###)
[//]: # ()
[//]: # (* Writing tests)
[//]: # (* Code review)
[//]: # (* Other guidelines)
[//]: # ()
[//]: # (### Who do I talk to? ###)
[//]: # ()
[//]: # (* Repo owner or admin)
[//]: # (* Other community or team contact)
[//]: # ()